package com.thechalakas.jay.listview;

/*
 * Created by jay on 17/09/17. 10:22 AM
 * https://www.linkedin.com/in/thesanguinetrainer/
 */

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class MainActivity extends AppCompatActivity
{

    //Get a data source
    //In this case, it will be a string array
    String[] arraySuperHeroes = {"Wonder Woman","Batman","Cyborg","Flash",
            "Aguaman","Superman"};

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Connect to a ListView Widget
        ListView listView = (ListView) findViewById(R.id.listview1);

        //Attach adapter to item view
        ArrayAdapter arrayAdapter = new ArrayAdapter<String>(this,R.layout.layoutforitem,arraySuperHeroes);

        //Attach adapter to list view
        listView.setAdapter(arrayAdapter);
    }
}
